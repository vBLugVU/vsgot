﻿function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


function toast_success(msg) {
    toast(msg, "weui-icon-success-no-circle");
}

function toast_info(msg) {
    toast(msg, "fa fa-info");
}

function toast_warning(msg) {
    toast(msg, "fa fa-exclamation");
}

function toast_doing(msg) {
    toast(msg, "weui-loading");
}

function toast(msg, cls) { // cls = weui-loading, weui_icon_*
    var $toast = $('#toast');
    if ($toast.length <= 0) {
        return;
    }

    if (!msg) {
        return;
    }
    msg = msg.replace(/\n/g, "<br/>");

    var $message = $toast.children("div:eq(1)").children("p");
    $message.html(msg);

    var $icon = $toast.children("div:eq(1)").children("i");
    $icon.removeClass();
    if (cls) {
        $icon.addClass(cls);
    }
    $icon.addClass("weui-icon_toast");

    $toast.show();
    setTimeout(function () {
        $toast.hide();
    }, 2000);
}

function untoast() {
    var $toast = $('#toast');
    $toast.hide();
}
